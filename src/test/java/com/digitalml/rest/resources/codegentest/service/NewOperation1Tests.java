package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.NewService.NewServiceServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.NewServiceService.NewOperation1InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.NewServiceService.NewOperation1ReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class NewOperation1Tests {

	@Test
	public void testOperationNewOperation1BasicMapping()  {
		NewServiceServiceDefaultImpl serviceDefaultImpl = new NewServiceServiceDefaultImpl();
		NewOperation1InputParametersDTO inputs = new NewOperation1InputParametersDTO();
		inputs.setRequest(org.apache.commons.lang3.StringUtils.EMPTY);
		NewOperation1ReturnDTO returnValue = serviceDefaultImpl._NewOperation_1(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}