package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class NewServiceTests {

	@Test
	public void testResourceInitialisation() {
		NewServiceResource resource = new NewServiceResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationNewOperation1NoSecurity() {
		NewServiceResource resource = new NewServiceResource();
		resource.setSecurityContext(null);

		Response response = resource._NewOperation_1(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationNewOperation1NotAuthorised() {
		NewServiceResource resource = new NewServiceResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource._NewOperation_1(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationNewOperation1Authorised() {
		NewServiceResource resource = new NewServiceResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource._NewOperation_1(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}